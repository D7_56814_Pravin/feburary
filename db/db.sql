create table user (
  id integer primary key auto_increment,
  name VARCHAR(200),
  password VARCHAR(200),
  email VARCHAR(50)
);

create table product (
  id integer primary key auto_increment,
  title VARCHAR(200),
  description VARCHAR(200),
  category VARCHAR(50),
  price float
);
const express = require('express')

const cors = require('cors')

const routerProduct = require('./routes/product')
const routerUser =require('./routes/user')

const app = express()
app.use(express.json())

app.use(cors('*'))


app.use('/product',routerProduct)
app.use('/user',routerUser)






app.listen(4000, () => {
    console.log(`server started on port 4000`)
  })